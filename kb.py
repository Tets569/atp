from symbols import *

VARS = 0
ANTECEDENT = 1
CONSEQUENT = 2

knowledge_base = (
	(['A', 'B'], ['A is a set', 'B is a set', 'A is a subset of B'], ['Every element in A is also in B']),
	(['A'], ['A is alone like me'], ['#ForeverAlone']),
	(['A', 'B'], ['A is NOT a set'], ['durrr'])
)


def seek(vars):
	results = []

	for knowledge in knowledge_base:
		print()
		print('=KNOWLEDGE=')

		print(f"vars: {knowledge[VARS]}")
		if all(var in knowledge[VARS] for var in vars):
			print(f"{CHECK_YES}")

			print(f"ant: {knowledge[ANTECEDENT]}")
			if all(input(f"{assumption}?") for assumption in knowledge[ANTECEDENT]):
				print(f"{CHECK_YES}")
				results.append(knowledge[CONSEQUENT])
			else:
				print(f"{CHECK_NO}")
				continue
		else:
			print(f"{CHECK_NO}")
			continue

	print(f'results: {results}')
	return results


if __name__ == '__main__':
	seek(['A', 'B'])

