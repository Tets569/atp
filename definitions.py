from symbols import *

if __name__ == "__main__":
	class EmptySet:
		__doc__ = f"""The set with no elements\n""" \
			f"""Let {EMPTY_SET} = {{{SCRIPT_LOWER_X}: {SCRIPT_LOWER_X} {NOT_EQUALS} {SCRIPT_LOWER_X}}}"""

	class Subset:
		__doc__ = f"""Every element of A is also an element of B\n""" \
			f"""A{SUBSET}B {IFF} ({FOR_ALL}{SCRIPT_LOWER_X})({SCRIPT_LOWER_X}{ELEMENT_OF}A {IMPLIES} {SCRIPT_LOWER_X}{ELEMENT_OF}B)"""

	class PowerSet:
		__doc__ = f"""Set whose elemetns are the subsets of A\n""" \
			f"""{SCRIPT_CAP_P}(A) = {{B: B{SUBSET}A}}"""

	class SetUnion:
		__doc__ = f"""Set of elements in A or B\n""" \
			f"""A{UNION}B = {{{SCRIPT_LOWER_X}: {SCRIPT_LOWER_X}{ELEMENT_OF}A or {SCRIPT_LOWER_X}{ELEMENT_OF}B}}"""

	class SetIntersection:
		__doc__ = f"""Set of elements in A and B\n""" \
			f"""A{INTERSECT}B = {{{SCRIPT_LOWER_X}: {SCRIPT_LOWER_X}{ELEMENT_OF}A and {SCRIPT_LOWER_X}{ELEMENT_OF}B}}"""

	class SetDifference:
		__doc__ = f"""Set of elements in A but not B\n""" \
			f"""A-B = {{{SCRIPT_LOWER_X}: {SCRIPT_LOWER_X}{ELEMENT_OF}A and {SCRIPT_LOWER_X}{NOT_ELEMENT_OF}B}}"""

	class SetDisjoint:
		__doc__ = f"""No elements are in both A and B\n""" \
			f"""'A and B are disjoint' {IFF} A{INTERSECT}B = {EMPTY_SET}"""

	class SetFinite:
		__doc__ = f"""Set has a finite number of elements\n""" \
			f"""'Set A is finite' {IFF} A={EMPTY_SET} or """ \
			f"""{EXISTS}{SCRIPT_LOWER_N}{ELEMENT_OF}{NUMBERS_NATURAL} s.t. A has {SCRIPT_LOWER_N} elements"""

	for a in [
		EmptySet,
		Subset,
		PowerSet,
		SetUnion,
		SetIntersection,
		SetDifference,
		SetDisjoint,
		SetFinite
	]:
		print()
		print(f"{a().__class__.__name__}: {a().__doc__}")
